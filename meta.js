module.exports = {
  "helpers": {
    "if_or": function (v1, v2, options) {
      if (v1 || v2) {
        return options.fn(this);
      }

      return options.inverse(this);
    }
  },
  "prompts": {
    "name": {
      "type": "string",
      "required": true,
      "message": "Project name"
    },
    "description": {
      "type": "string",
      "required": false,
      "message": "Project description",
      "default": "A React.js project"
    },
    "author": {
      "type": "string",
      "message": "Author"
    },
    "lint": {
      "type": "confirm",
      "message": "Use ESLint to lint your code?"
    },
    "unit": {
      "type": "confirm",
      "message": "Setup unit tests with Mocha + assert?"
    },
    "reactRouter": {
      "type": "confirm",
      "message": "Use react-router in your code?"
    },
    "redux": {
      "type": "confirm",
      "message": "Use redux in your code?"
    },
    "style": {
      "type": "list",
      "message": "Use less or sass in your code?",
      "choices": [
        {
          "name": "Use less",
          "value": "less",
          "short": "less"
        },
        {
          "name": "Use sass",
          "value": "sass",
          "short": "sass"
        },
        {
          "name": "just use css",
          "value": "css",
          "short": "css"
        }
      ]
    }
  },
  "filters": {
    ".eslintrc": "lint",
    ".eslintignore": "lint",
    "test": "unit",
    "redux": "redux",
    "routes": "routes"
  },
  "completeMessage": "done"
};